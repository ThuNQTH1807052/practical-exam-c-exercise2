#include <stdio.h>
#include <string.h>

int main() {
    char string[100];
    printf("Enter string: ");
    fgets(string, sizeof(string)* sizeof(char), stdin);

    int i, demA = 0, demE = 0, demI = 0, demO = 0, demU = 0, demRest= 0;
    printf("Number of character:\n");
    for (i = 0 ; i<strlen(string)-1; i++ ) {
        if (string[i] == 'a') {
            demA++;
        }
        else if (string[i] == 'e') {
            demE++;
        }
        else if (string[i] == 'i') {
            demI++;
        }
        else if (string[i] == 'o') {
            demO++;
        }
        else if (string[i] == 'u') {
            demU++;
        }
        else{
            demRest++ ;
        }
    }
    printf("a: %d; e: %d; i: %d; o: %d; u: %d; rest: %d", demA, demE, demI, demO, demU, demRest);

    printf("\nPercentages of total:\n");
    float percentA, percentE, percentI, percentO, percentU, percentRest;
    percentA = (float)demA/(strlen(string)-1)*100;
    percentE = (float)demE/(strlen(string)-1)*100;
    percentI = (float)demI/(strlen(string)-1)*100;
    percentO = (float)demO/(strlen(string)-1)*100;
    percentU = (float)demU/(strlen(string)-1)*100;
    percentRest = (float)demRest/(strlen(string)-1)*100;
    printf("a: %.0f%%; e: %.0f%%; i: %.0f%%; o: %.0f%%; u: %.0f%%; rest: %.0f%%",
           percentA, percentE, percentI, percentO, percentU, percentRest);
    return 0;
}